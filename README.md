# yunohost_script

Script pour générer automatiquement des containers yunohost avec docker et docker compose  
permet de créer plusieurs container à la suite.  

Tester sur windows 64bit (amd64), ubuntu 64bit(amd64), raspberry os 64bit (arm64)

Ce script à été créer grâce au script generator de xavki :
https://gitlab.com/xavki/presentations-dockercompose/-/blob/master/21-utils-script-grafana/generator.sh

et au dépot de domainelibre pour créer des image yunohost avec Docker :
https://github.com/domainelibre/YunohostDockerImage

Merci a vous deux !

| Options       | Description      |
| ------------- | ---------------- |
| -h ou --help    | affiche les commande possible ? |
| -i ou --ip | liste les addresse ip et ports pour chaque container |
| -y ou --yunohost | lance un container yunohost |

## Installation pour windows

Télécharger git :
https://git-scm.com/downloads

Télécharger docker :
https://hub.docker.com/editions/community/docker-ce-desktop-windows/


### clone du dépot et lancement du script

Faire un clique droit dans un dossier et selectionner "Git Bash Here" et cloner le depot du script
```bash
git clone https://gitlab.com/MaximeDev/yunohost_script.git
```

Ensuite faire un clique droit dans le dossier télécharger et selectionner "Git Bash Here"  
!!Ce script est un script linux et ne se lance pas avec cmd ou powershell!!

puis faire la commande : (en n'oubliant pas le "./" avant le nom du script)
```bash
./script.sh -y
```

## Utilisation du script

pour help
```bash
./script.sh -h
```

pour lancer un container yunohost
```bash
./script.sh -y
```

pour lister les addresses ip et ports de chaque container
```bash
./script.sh -i
```

## réglage pour windows
pour utiliser avec windows il faut rajouter une route
1) pour cela on récupere le sous réseaux dans lequel est le container
Faire :
```bash
./script.sh -i
```
cela nous donne l'addresse ip du contener que l'on souhaite récuperer  
ex :"frontend//172.21.0.2"  
donc le sous réseaux de cette addresse sera : 172.21.0.0/16  

2) On récupere ensuite l'adresse ip addresse de notre pc 
avec cmd faire :
```bash
ipconfig
```
ex "Carte réseau sans fil Wi-Fi :  
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.2.130  "  
3) Ensuite on ajoute la route dans windows avec cmd avec les privileges administrateur
```bash
route add 172.21.0.0/16 192.168.2.130
```

## Installation pour linux

il faut avant installer git,docker and docker-compose  

https://git-scm.com/downloads
https://docs.docker.com/engine/install/

### Installation Debian
```bash
sudo apt update && apt upgrade
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common \
    git
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
```
remplacer l'architecture en fonction de votre machine
[arch=amd64][arch=armhf] [arch=arm64]
```bash
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
```
```bash
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```
```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```
### Installation Ubuntu
```bash
sudo apt update && apt upgrade
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common \
    git
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
```
remplacer l'architecture en fonction de votre machine
[arch=amd64][arch=armhf] [arch=arm64]
```bash
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```
```bash
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```
### clone du dépot et lancement du script

```bash
git clone https://gitlab.com/MaximeDev/yunohost_script.git
cd yunohost_script
chmod u+x script.sh
```

## Utilisation du script

pour help
```bash
sudo ./script.sh -h
```

pour lancer un container yunohost
```bash
sudo ./script.sh -y
```

pour lister les addresses ip et ports de chaque container
```bash
sudo ./script.sh -i
```