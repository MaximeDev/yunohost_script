#!/bin/bash


DIR="${PWD}"
USER_SCRIPT=$USER
args=("$@")
# Fonctions ###########################################################

help_list() {
  echo "Usage:

  ${0##*/} [-h][--prometheus][--grafana]

  Options:

    -h, --help
      affiche les commandes possible

    -i, --ip
      liste les addresse ip pour chaque conteneur
    
    -y, --yunohost
      lance un conteneur yunohost
    "
}

parse_options() {
  case ${args[0]} in
    -h|--help)
      help_list
      exit
      ;;
    -i|--ip)
      ip
      ;;
    -y|--yunohost)
      yunohost
      ;;

    *)
      echo "Option inconnu: ${opt} - lancer la commande ' ${0##*/} -h ' pour afficher les commandes possible.">&2
      exit 1
  esac
}

check() {
  checksubnet
  checkid
  checkport
}

checkid() {
  clear
  echo
  echo "Traitement en cours..."
  echo
  echo "check id"
  idmax=`docker ps -a --format '{{ .Names}}' | awk -F "-" -v app="$appname" '$0 ~ app"-" {print $2}' | sort -r |head -1`
  if [ ! -z "$idmax" ]; then 
      clear
      echo
      echo "Un ou plusieur conteneur existe déja, voulez-vous en lancer un existant ou en créer un nouveau ? ";
      echo
      echo "Pour lancer un conteneur déja existant colle le nom d'un des conteneurs qui s'affiche tout en haut ex:yunohost-1"
      echo
      docker ps -a --format '{{ .Names}}' | awk -F "-" -v app="$appname" '$0 ~ app"-" {print $0}' | sort -r
      echo
      echo "sinon appuiez sur entrer..."
      read app
        if [ ! -z "$app" ]; then 
        docker-compose -f $DIR/$app/docker-compose.yml up -d
        exit
      else id=$(($idmax + 1));
      fi
  else id=$(($idmax + 1));
  fi
}

checkport() {
  clear
  echo
  echo "Traitement en cours..."
  echo
  echo "check ports"
 port22=$(for i in $(docker ps -q); do docker port $i 22 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port25=$(for i in $(docker ps -q); do docker port $i 25 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port53udp=$(for i in $(docker ps -q); do docker port $i 53/udp 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port80=$(for i in $(docker ps -q); do docker port $i 80 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port443=$(for i in $(docker ps -q); do docker port $i 443 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port137=$(for i in $(docker ps -q); do docker port $i 137 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port138=$(for i in $(docker ps -q); do docker port $i 138 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port139=$(for i in $(docker ps -q); do docker port $i 139 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port465=$(for i in $(docker ps -q); do docker port $i 465 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port587=$(for i in $(docker ps -q); do docker port $i 587 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port993=$(for i in $(docker ps -q); do docker port $i 993 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port3000=$(for i in $(docker ps -q); do docker port $i 3000 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port3306=$(for i in $(docker ps -q); do docker port $i 3306 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port5000=$(for i in $(docker ps -q); do docker port $i 5000 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port5222=$(for i in $(docker ps -q); do docker port $i 5222 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port5432=$(for i in $(docker ps -q); do docker port $i 5432 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port5269=$(for i in $(docker ps -q); do docker port $i 5269 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port8080=$(for i in $(docker ps -q); do docker port $i 8080 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port9000=$(for i in $(docker ps -q); do docker port $i 9000 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port9091=$(for i in $(docker ps -q); do docker port $i 9091 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )
 port49200=$(for i in $(docker ps -q); do docker port $i 49200 2>/dev/null | awk -F ":" '{ $1 = "" ; print $0 }'; done | sort -r | head -1 )

if [ -z "$port22" ]; then port22=22 ; else port22=$(($port22 + 1)); fi
if [ -z "$port25" ]; then port25=25 ; else port25=$(($port25 + 1)); fi
if [ -z "$port53udp" ]; then port53udp=53 ; else port53udp=$(($port53udp + 1)); fi
if [ -z "$port80" ]; then port80=80 ; else port80=$(($port80 + 1)); fi
if [ -z "$port137" ]; then port137=137 ; else port137=$(($port137 + 3)); fi
if [ -z "$port138" ]; then port138=138 ; else port138=$(($port138 + 3)); fi
if [ -z "$port139" ]; then port139=139 ; else port139=$(($port139 + 3)); fi
if [ -z "$port443" ]; then port443=443 ; else port443=$(($port443 + 3)); fi
if [ -z "$port465" ]; then port465=465 ; else port465=$(($port465 + 1)); fi
if [ -z "$port587" ]; then port587=587 ; else port587=$(($port587 + 1)); fi
if [ -z "$port993" ]; then port993=993; else port993=$(($port993+ 1)); fi
if [ -z "$port3000" ]; then port3000=3000 ; else port3000=$(($port3000 + 1)); fi
if [ -z "$port3306" ]; then port3306=3306 ; else port3306=$(($port3306 + 1)); fi
if [ -z "$port5000" ]; then port5000=5000 ; else port5000=$(($port5000 + 1)); fi
if [ -z "$port5222" ]; then port5222=5222 ; else port5222=$(($port5222 + 1)); fi
if [ -z "$port5269" ]; then port5269=5269 ; else port5269=$(($port5269 + 1)); fi
if [ -z "$port5432" ]; then port5432=5432 ; else port5432=$(($port5432 + 1)); fi
if [ -z "$port8080" ]; then port8080=8080 ; else port8080=$(($port8080 + 1)); fi
if [ -z "$port9000" ]; then port9000=9000 ; else port9000=$(($port9000 + 1)); fi
if [ -z "$port9091" ]; then port9091=9091 ; else port9091=$(($port9091 + 1)); fi
if [ -z "$port49200" ]; then port49200=49200 ; else port49200=$(($port49200 + 1)); fi
}

checksubnet() {
  clear
  echo
  echo "Traitement en cours..."
  echo
  echo "check sous-réseaux"
  listNetwork=`docker network ls -f type=custom --format "{{.Name}}: {{.Driver}}"`
  if [ ! -z "$listNetwork" ]; then
    clear
    echo
    echo "Un ou plusieurs réseaux existe déja, voulez-vous en uttiliser un existant ou créer un nouveau réseau ? ";
    echo
    echo "Si aucun réseaux n'est selectionné ni créé alors le réseau 'frontend' sera uttiliser par deault"
    echo
    echo "Pour utiliser un réseau déja existant autre que le réseau par défaut, collez le nom du reseaux qui s'affiche ex : frontend"
    echo
    echo $listNetwork
    echo
    echo "sinon appuiez sur entrer..."
    read name_subnet
    if [ ! -z "$network" ]; then
      continue
    else 
      subnet
    fi
  else 
    subnet
  fi
}

subnet() {
  clear
    echo "le conteneur $appname à besoin d'un réseaux afin d'acceder au conteneur depuis un navigateur."
    echo
    echo "un réseaux par défaut 'frontend' sera créé si vous n'en créez pas un et si il n'existe pas"
    echo
    echo "Voulez vous créer un réseaux personalisé ? : répondre avec la touche 'O' ou laisser par défaut en appuyant sur n'importe quel autre touche."
    read choice
    if [[ "$choice" ==  [oO] ]]; then
      echo "Veuillez saisir un nom de réseaux ex: frontend"
      read name_subnet
      echo "Veuillez saisir un sous-réseaux ip ex: 172.26.1.0 (un sous-réseau fini toujours par .0)"
      until [[ ${subnet} =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0]$ ]]; do
        read subnet
      done
      docker network create --subnet=$subnet/24 $name_subnet
      docker network inspect $name_subnet -f "{{range .IPAM.Config}}{{.Subnet}}{{end}} - {{.Name}}"
    else
     echo "Le réseaux frontend sera créer par défaut"
      name_subnet=frontend
      docker network create $name_subnet
      docker network inspect $name_subnet -f "{{range .IPAM.Config}}{{.Subnet}}{{end}} - {{.Name}}"
      sleep 1s
    fi
}

ip() {
  for i in $(docker ps -q); do
    (( docker inspect $i -f "{{.HostConfig.NetworkMode}}//{{range .NetworkSettings.Networks}}{{.IPAddress}} - {{end}}{{.Name}} -" ; docker port $i ) | paste -d" " -s ) ;done
}

yunohost() {
    
  appname=yunohost
  check

  clear
  echo
  echo " Entrez un nom de domaine ex : exemple.fr"
  read domain

  echo
  echo "Installation de $appname-$id"

  echo "1 - Création des dossiers ${DIR}/$appname-$id/{media,hack_scripts,backup,log}"
  mkdir -p $DIR/$appname-$id/{media,hack_scripts,backup,log}

  echo "2 - Création du fichier hostfiles-hack.service"
  echo "[Unit]
After=sshd.service
[Service]
ExecStart=/usr/local/bin/hostfiles-hack.sh
[Install]
WantedBy=default.target
" > $DIR/$appname-$id/hack_scripts/hostfiles-hack.service

  echo "3 Création du fichier hostfiles-hack.sh"
  echo "#!/bin/bash
TAG=\"#HACKED\"
# hack /etc/hosts
grep -q \"^\$TAG\" /etc/hosts
if [ \"\$1\" != \"0\" ]
then
	cp /etc/hosts /etc/hosts_cp
	sed -i \"1i\$TAG\" /etc/hosts_cp
	umount /etc/hosts
	cp /etc/hosts_cp /etc/hosts
fi
# hack /etc/hostname
cp /etc/hostname /etc/hostname_cp
umount /etc/hostname
cp /etc/hostname_cp /etc/hostname
" > $DIR/$appname-$id/hack_scripts/hostfiles-hack.sh

  echo "4 Création du fichier preinstall.sh"
  echo "#!/bin/bash
DIRORI=$(dirname \$0)
cd \$DIRORI
# install type
INSTALL_TYPE=stable
[ \"\$1\" != \"\" ] && INSTALL_TYPE=\$1
# branche_type
BRANCHE_TYPE=buster
[ \"\$2\" != \"\" ] && BRANCHE_TYPE=\$2
# install requirements
# deprecated : --force-yes
apt-get update --quiet
apt-get install -y --no-install-recommends wget apt-utils ssh openssl ca-certificates openssh-server nano vim cron git
# debug docker context for resolvconf
# deprecated : --force-yes
apt-get install -y --no-install-recommends resolvconf 2>/dev/null || \\
 echo \"resolvconf resolvconf/linkify-resolvconf boolean false\" | debconf-set-selections
# get yunohost git repo
git clone -b \$BRANCHE_TYPE https://github.com/YunoHost/install_script /tmp/install_script
git -C /tmp/install_script checkout \$BRANCHE_TYPE
# hack YunoHost install_script for bypass systemd check
sed -i \"s@/run/systemd/system@/run@g\" /tmp/install_script/install_yunohost
# debug systemctl issues for docker, add proxy
mv /bin/systemctl /bin/systemctl.bin
echo -e \"#\"'!'\"/bin/sh\n/bin/./systemctl.bin\nexit 0\n\" > /bin/systemctl
chmod +x /bin/systemctl
# force ulimit for slapd now
ulimit -n 1024
# do yunohost installation
cd /tmp/install_script
./install_yunohost -f -a -d \$INSTALL_TYPE
[ \"\$?\" != \"0\" ] && echo \"error while yunohost installation\" && exit 1
# hack iptables for yunohost in docker
mv /usr/sbin/iptables /usr/sbin/iptables.ori
echo -e \"#\"'!'\"/bin/sh\necho \"fake iptables for yunohost inside docker, unusable. For return non failure unix code 0. Bye !\"\nexit 0\n\" > /usr/sbin/iptables
chmod +x /usr/sbin/iptables
# remove proxy for systemctl
rm -f /bin/systemctl
mv /bin/systemctl.bin /bin/systemctl
# force ulimit for slapd in starting script
sed -i '/\/lib\/lsb\/init-functions/a ulimit -n 1024' /etc/init.d/slapd
# patchs for yunohost
adduser admin
systemctl enable nginx
systemctl enable yunohost-api
systemctl enable php7.3-fpm
systemctl enable fail2ban
[ ! -f /etc/fail2ban/filter.d/sshd-ddos.conf ] \\
	&& echo -e \"[Definition]\n\" > /etc/fail2ban/filter.d/sshd-ddos.conf
[ ! -f /etc/fail2ban/filter.d/postfix-sasl.conf ] \\
        && echo -e \"[Definition]\n\" > /etc/fail2ban/filter.d/postfix-sasl.conf
touch /var/log/mail.log
#systemctl enable dovecot
#systemctl enable postfix
#systemctl enable rspamd
#systemctl enable avahi-daemon
#systemctl enable dnsmasq
#systemctl enable redis-server
# cleaning
apt-get clean
" > $DIR/$appname-$id/preinstall.sh
  
  echo "5 - Création du fichier Dockerfile "
echo "
FROM debian:10
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -y systemd && apt-get clean
ADD preinstall.sh /tmp/preinstall.sh
RUN chmod +x /tmp/preinstall.sh
RUN /tmp/./preinstall.sh && rm /tmp/preinstall.sh && apt-get clean && apt-get autoclean
ADD hack_scripts/hostfiles-hack.sh /usr/local/bin/
ADD hack_scripts/hostfiles-hack.service /etc/systemd/system/
RUN chmod 664 /etc/systemd/system/hostfiles-hack.service && \
 chmod 744 /usr/local/bin/hostfiles-hack.sh && \
 systemctl enable hostfiles-hack.service
CMD [\"/bin/systemd\"]
"> $DIR/$appname-$id/Dockerfile

  echo "6 - Création du fichier $appname-$id.yml "
echo "
version: '3.5'
services:
  $appname-$id:
    build:
      context: .
    privileged: true
    container_name: $appname-$id
    restart: always
    ports:
      - $port22:22
      - $port25:25
      - $port53udp:53/udp
      - $port80:80
      - $port137:137
      - $port138:138
      - $port139:139
      - $port443:443
      - $port465:465
      - $port587:587
      - $port993:993
      - $port5222:5222
      - $port5269:5269
      - $port49200:49200
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - $appname-$id-media:/media
      - /sys/fs/cgroup:/sys/fs/cgroup:ro
      - $appname-$id-backup:/home/yunohost.backup
      - $appname-$id-log:/var/log/
    labels:
      - traefik.enable=true
      - traefik.http.routers.$appname-$id-frontend.rule=Host(\`$appname-$id.$domain\`)
      - traefik.http.routers.$appname-$id-frontend.entrypoints=secureweb
      - traefik.http.services.$appname-$id-frontend.loadbalancer.server.port=80
      - traefik.http.routers.$appname-$id-frontend.service=$appname-$id-frontend
      - traefik.http.routers.$appname-$id-frontend.tls.certresolver=cert
    hostname: $domain
    networks:
      frontend:
          aliases:
           - $domain
volumes:
  $appname-$id-media:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ${DIR}/$appname-$id/media
  $appname-$id-log:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ${DIR}/$appname-$id/log
  $appname-$id-backup:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ${DIR}/$appname-$id/backup
networks:
  frontend:
    external:
      name: $name_subnet
" > $DIR/$appname-$id/docker-compose.yml
  
  echo "4 - Lancement du conteneur $appname-$id "
  docker-compose -f $DIR/$appname-$id/docker-compose.yml up -d

  ip
}

# Let's Go !! parse args  ####################################################################

parse_options $@